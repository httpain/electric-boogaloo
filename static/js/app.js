var sampleData = [
    {
        "date": "23.03.2018",
        "day": 1801,
        "night": 617
    },
    {
        "date": "05.04.2018",
        "day": 1919,
        "night": 640
    },
    {
        "date": "15.05.2018",
        "day": 2157,
        "night": 749
    },
    {
        "date": "17.07.2018",
        "day": 2567,
        "night": 901
    },
    {
        "date": "23.08.2018",
        "day": 2901,
        "night": 1013
    },
    {
        "date": "28.10.2018",
        "day": 3334,
        "night": 1231
    },
    {
        "date": "17.11.2018",
        "day": 3519,
        "night": 1311
    },
    {
        "date": "09.12.2018",
        "day": 3689,
        "night": 1399
    }
];

new Vue({
    el: '#app',
    data: {
        measurements: []
    },
    methods: {
        onPaste: function (e) {
            this.measurements = [];
            e.preventDefault();
            e.stopPropagation();
            var pastedHtml = e.clipboardData.getData('text/html');
            var parser = new DOMParser();
            var root = parser.parseFromString(pastedHtml, 'text/html');
            var rows = [].slice.call(root.getElementsByTagName('tr'));
            var vue = this;
            rows.forEach(function (row) {
                var cellData = [].slice.call(row.getElementsByTagName('td')).map(function (td) {
                    return td.innerHTML.trim()
                });
                console.log();
                var parts = cellData[1].split('/');
                var day = parseFloat(parts[0].trim());
                var night = parseFloat(parts[1].trim());
                vue.measurements.unshift(
                    {
                        date: cellData[0],
                        day: day,
                        night: night
                    }
                );
            });
        },
        fillSampleData: function () {
            this.measurements = [];
            this.measurements = sampleData
        }
    },
    watch: {
        measurements: function () {
            var config = {
                type: 'line',
                data: {
                    datasets: [
                        {
                            label: 'День',
                            backgroundColor: '#69D2E7',
                            borderColor: '#69D2E7',
                            borderWidth: 2,
                            fill: false,
                            data: this.measurements.map(function (measurement) {
                                return {
                                    x: moment(measurement.date, 'DD.MM.YYYY'),
                                    y: measurement.day
                                }
                            })
                        },
                        {
                            label: 'Ночь',
                            backgroundColor: '#F38630',
                            borderColor: '#F38630',
                            borderWidth: 2,
                            fill: false,
                            data: this.measurements.map(function (measurement) {
                                return {
                                    x: moment(measurement.date, 'DD.MM.YYYY'),
                                    y: measurement.night
                                }
                            })
                        },
                        {
                            label: 'Сумма',
                            backgroundColor: '#d3d7bf',
                            borderColor: '#d3d7bf',
                            borderWidth: 2,
                            fill: false,
                            data: this.measurements.map(function (measurement) {
                                return {
                                    x: moment(measurement.date, 'DD.MM.YYYY'),
                                    y: measurement.day + measurement.night
                                }
                            })
                        }
                    ]
                },
                options: {
                    title: {
                        text: 'My Title'
                    },
                    scales: {
                        xAxes: [{
                            type: 'time',
                            time: {
                                format: 'DD.MM.YYYY'
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                min: 0
                            }
                        }]
                    },
                    animation: {
                        // easing: 'easeOutBounce'
                    }
                }
            };
            var ctx = document.getElementById("chart");
            new Chart(ctx, config);
        }
    }
});
